package com.telstra.service;

import com.telstra.entity.ConfigurationEntity;


/**
 * Created by raman on 01/05/18
 */
public interface IConfigurationService {
    void save(ConfigurationEntity configuration);

   // int getTimeToLive();
    ConfigurationEntity getTimeToLive();

}
