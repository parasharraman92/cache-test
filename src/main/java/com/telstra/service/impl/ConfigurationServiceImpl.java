package com.telstra.service.impl;

import com.telstra.dao.IConfigurationRepository;
import com.telstra.entity.ConfigurationEntity;
import com.telstra.service.IConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by raman on 01/05/18
 */
@Service
public class ConfigurationServiceImpl implements IConfigurationService {

    @Autowired
    private IConfigurationRepository configurationRepository;

    @Override
    public void save(ConfigurationEntity configuration) {
        configurationRepository.save(configuration);
    }

    @Override
    public ConfigurationEntity getTimeToLive() {
        return configurationRepository.findAll().get(0);
    }
}
