package com.telstra.dao;

import com.telstra.entity.ConfigurationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by raman on 01/05/18
 */
public interface IConfigurationRepository extends JpaRepository<ConfigurationEntity,Integer> {
}
